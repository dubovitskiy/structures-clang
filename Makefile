stacklist:
	@mkdir --parents tests/
	@rm --recursive --force tests/stack_test
	gcc structures/linear/stack/stack_list_test.c \
		structures/linear/stack/stack_list.c \
		structures/linear/stack/stack_list.h \
		-l cmocka \
		-o tests/stack_test \
		-v \
	&& ./tests/stack_test

bst:
	@mkdir --parents tests/
	@rm --recursive --force tests/bst_test
	gcc structures/recursive/bst/tree_test.c \
		structures/recursive/bst/tree.c \
		structures/recursive/bst/tree.h \
		-l cmocka \
		-o tests/bst_test \
		-v \
	&& ./tests/bst_test

qa:
	@mkdir --parents tests/
	@rm --recursive --force tests/qa_test
	gcc structures/linear/queue_array/queue_test.c \
		structures/linear/queue_array/queue.c \
		structures/linear/queue_array/queue.h \
		-l cmocka \
		-o tests/qa_test \
		-v \
	&& ./tests/qa_test

ql:
	@mkdir --parents tests/
	@rm --recursive --force tests/ql_test
	gcc structures/linear/queue_list/queue_test.c \
		structures/linear/queue_list/queue.c \
		structures/linear/queue_list/queue.h \
		-l cmocka \
		-o tests/ql_test \
		-v \
	&& ./tests/ql_test

qs:
	@mkdir --parents tests/
	@rm --recursive --force tests/ql_test
	gcc structures/linear/queue_stack/queue.c \
		structures/linear/queue_stack/queue.h \
		structures/linear/stack/stack_list.c \
		structures/linear/stack/stack_list.h \
		structures/linear/queue_stack/queue_test.c \
		-l cmocka \
		-I$$(pwd)/structures/linear/stack \
		-o tests/qs_test \
		-v \
	&& ./tests/qs_test
clean:
	@find . -name *.out -or -name *.gch | xargs rm -f
	@rm --recursive --force tests/
