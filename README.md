# Data structures

I'm not much of a C programmer, so I've decided
to learn it from scratch by solving problems with
data structures and algorythms on weekends when I
have a time for it.

Install dependencies to get tests working

``` bash
sudo dnf install make gcc libcmocka libcmocka-devel
```
