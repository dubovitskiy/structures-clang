#include <stdlib.h>
#include <stdbool.h>
#include "queue.h"

// Source - https://www.geeksforgeeks.org/queue-set-2-linked-list-implementation/

QNode *queue_node_create(int data)
{
    QNode *node = (QNode *)malloc(sizeof(QNode));

    node->data = data;
    node->next = NULL;

    return node;
}

Queue *queue_create()
{
    Queue *queue = (Queue *)malloc(sizeof(Queue));

    queue->front = NULL;
    queue->rear = NULL;

    return queue;
}

bool queue_is_empty(Queue *queue)
{
    return queue->front == NULL;
}

void queue_enqueue(Queue *queue, int data)
{
    QNode *node = queue_node_create(data);

    if (queue_is_empty(queue))
    {
        queue->front = node;
        queue->rear = node;
        return;
    }

    queue->rear->next = node;
    queue->rear = node;
}

int queue_dequeue(Queue *queue)
{
    if (queue_is_empty(queue))
    {
        return NULL;
    }

    QNode *node = queue->front;

    queue->front = queue->front->next;

    if (queue->front == NULL)
    {
        queue->rear = NULL;
    }

    int data = node->data;
    free(node);

    return data;
}
