typedef struct QNode
{
    // Payload
    int data;

    // Pointer to the next element
    struct QNode *next;

} QNode;

typedef struct Queue
{
    // Pointer to the front element
    struct QNode *front;

    // Pointer to the rear element
    struct QNode *rear;

} Queue;

Queue *queue_create();

QNode *queue_node_create(int data);

void queue_enqueue(Queue *queue, int data);

int queue_dequeue(Queue *queue);
