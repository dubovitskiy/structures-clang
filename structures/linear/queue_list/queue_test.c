#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "queue.h"

static void test_create_queue_node()
{
    QNode *node = queue_node_create(10);

    assert_int_equal(node->data, 10);
    assert_null(node->next);
}

static void test_create_queue()
{
    Queue *queue = queue_create(10);

    assert_null(queue->front);
    assert_null(queue->rear);
}

static void test_enqueue()
{
    Queue *queue = queue_create(5);
    queue_enqueue(queue, 10);

    assert_int_equal(queue->rear->data, 10);
    assert_int_equal(queue->front->data, 10);

    queue_enqueue(queue, 100);

    assert_int_equal(queue->rear->data, 100);
    assert_int_equal(queue->front->data, 10);

    queue_enqueue(queue, 50);

    assert_int_equal(queue->rear->data, 50);
    assert_int_equal(queue->front->data, 10);
}

static void test_dequeue() {
    Queue *queue = queue_create(10);
    queue_enqueue(queue, 1);
    queue_enqueue(queue, 2);
    queue_enqueue(queue, 3);
    queue_enqueue(queue, 4);
    queue_enqueue(queue, 5);

    assert_int_equal(queue_dequeue(queue), 1);
    assert_int_equal(queue_dequeue(queue), 2);
    assert_int_equal(queue_dequeue(queue), 3);
    assert_int_equal(queue_dequeue(queue), 4);
    assert_int_equal(queue_dequeue(queue), 5);
}

int main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_create_queue_node),
        cmocka_unit_test(test_create_queue),
        cmocka_unit_test(test_enqueue),
        cmocka_unit_test(test_dequeue),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
