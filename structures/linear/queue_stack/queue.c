#include <stdbool.h>
#include "queue.h"
#include "stack_list.h"

Queue *queue_create()
{
    Queue *queue = (Queue *)malloc(sizeof(Queue));

    queue->left_stack = stack_new();
    queue->right_stack = stack_new();

    return queue;
}

void queue_enqueqe(Queue *queue, int data)
{
    stack_push(queue->left_stack, data);
}

void _move_elements(Queue *queue)
{
    while (!stack_is_empty(queue->left_stack))
    {
        // Pop an element from the left stack
        int data = stack_pop(queue->left_stack);

        // And push it to the right stack
        stack_push(queue->right_stack, data);
    }
}

int queue_dequeue(Queue *queue)
{
    if (stack_is_empty(queue->right_stack))
    {
        _move_elements(queue);
    }
    return stack_pop(queue->right_stack);
}

bool queue_is_empty(Queue *queue)
{
    bool left_stack_empty = stack_is_empty(queue->left_stack);
    bool right_stack_empty = stack_is_empty(queue->right_stack);

    return left_stack_empty && right_stack_empty;
}
