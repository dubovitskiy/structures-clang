#include <stdbool.h>

typedef struct Queue {
    struct ll_stack *left_stack;
    struct ll_stack *right_stack;
} Queue;

Queue *queue_create();

void queue_enqueqe(Queue *queue, int data);

int queue_dequeue(Queue *queue);

bool queue_is_empty(Queue *queue);
