#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "queue.h"


static void test_create_queue()
{
    Queue *queue = queue_create();
    assert_true(stack_is_empty(queue->left_stack));
    assert_true(stack_is_empty(queue->right_stack));
}

static void test_queue_enqueue()
{
    Queue *queue = queue_create();

    queue_enqueqe(queue, 10);

    assert_false(stack_is_empty(queue->left_stack));
    assert_int_equal(stack_pop(queue->left_stack), 10);
}

static void test_queue_dequeue()
{
    Queue *queue = queue_create();

    queue_enqueqe(queue, 1);
    queue_enqueqe(queue, 2);
    queue_enqueqe(queue, 3);
    queue_enqueqe(queue, 4);

    assert_int_equal(stack_length(queue->left_stack), 4);

    int data = queue_dequeue(queue);

    assert_int_equal(data, 1);

    // All elements moved to the right stack
    assert_int_equal(stack_length(queue->left_stack), 0);
    assert_int_equal(stack_length(queue->right_stack), 3);
}

static void test_queue_is_empty()
{
    Queue *queue = queue_create();

    assert_true(queue_is_empty(queue));

    queue_enqueqe(queue, 1);

    assert_false(queue_is_empty(queue));

    queue_dequeue(queue);

    assert_true(queue_is_empty(queue));
}

int main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_create_queue),
        cmocka_unit_test(test_queue_enqueue),
        cmocka_unit_test(test_queue_dequeue),
        cmocka_unit_test(test_queue_is_empty),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
