#include <stdbool.h>
#include <stdlib.h>

struct ll_stack_node
{
    // Payload
    int data;

    // Pointer to the next node
    struct ll_stack_node *next;
};

struct ll_stack
{
    // Length of the stack
    int length;

    // Pointer to the top of the stack
    struct ll_stack_node *top;
};

struct ll_stack *stack_new();
struct ll_stack_node *stack_node_new(int data);
void stack_push(struct ll_stack *s, int data);
int stack_length(struct ll_stack *s);
bool stack_is_empty(struct ll_stack *s);
int stack_pop(struct ll_stack *s);
