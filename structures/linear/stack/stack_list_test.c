#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "stack_list.h"

static void test_create_stack()
{
    struct ll_stack *stack = stack_new();
    assert_int_equal(stack->length, 0);
    assert_null(stack->top);
}

static void test_stack_length()
{
    struct ll_stack *stack = stack_new();
    assert_int_equal(stack_length(stack), 0);
    stack_push(stack, 2);
    assert_int_equal(stack_length(stack), 1);
}

static void test_stack_push()
{
    struct ll_stack *stack = stack_new();

    stack_push(stack, 1);
    assert_int_equal(stack->top->data, 1);

    stack_push(stack, 2);
    assert_int_equal(stack->top->data, 2);
}

static void test_stack_pop()
{
    struct ll_stack *stack = stack_new();

    stack_push(stack, 200);
    stack_push(stack, 100);

    assert_int_equal(stack->top->data, 100);
    assert_int_equal(stack_length(stack), 2);

    int hundred = stack_pop(stack);
    assert_int_equal(hundred, 100);
    assert_int_equal(stack_length(stack), 1);

    int two_hundreds = stack_pop(stack);
    assert_int_equal(two_hundreds, 200);
    assert_int_equal(stack_length(stack), 0);
}

static void test_stack_is_empty()
{
    struct ll_stack *stack = stack_new();

    assert_true(stack_is_empty(stack));

    stack_push(stack, 99);

    assert_false(stack_is_empty(stack));
}

int main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_create_stack),
        cmocka_unit_test(test_stack_length),
        cmocka_unit_test(test_stack_pop),
        cmocka_unit_test(test_stack_is_empty),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
