#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "stack_list.h"

struct ll_stack *stack_new()
{
    struct ll_stack *s = malloc(sizeof(struct ll_stack));

    // Initialization
    s->length = 0;
    s->top = 0;
    return s;
}

struct ll_stack_node *stack_node_new(int data)
{
    struct ll_stack_node *node = malloc(sizeof(struct ll_stack_node));

    // Initialization
    node->data = data;
    node->next = NULL;
    return node;
}

void stack_push(struct ll_stack *s, int data)
{
    struct ll_stack_node *node = stack_node_new(data);

    // Set current top to the new node object
    // as the next node if stack is not empty
    if (s->top != NULL)
    {
        node->next = s->top;
    }

    // Increase count
    s->length++;

    // And then set the new top
    s->top = node;
}

int stack_length(struct ll_stack *s)
{
    return s->length;
}

bool stack_is_empty(struct ll_stack *s)
{
    return s->length == 0;
}

int stack_pop(struct ll_stack *s)
{
    // Stack underflow
    if (stack_is_empty(s))
    {
        return -1; // Workaround
    }

    struct ll_stack_node *node = s->top;

    // Get a result
    int data = node->data;

    // Set new top of the stack
    s->top = node->next;

    // Free up memory
    free(node);

    s->length--;
    return data;
}
