#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>

typedef struct tree_node
{
    int data;
    struct tree_node *left;
    struct tree_node *right;
} tree_node;

tree_node *tree_create(int data);

bool tree_lookup(tree_node *node, int data);

tree_node *tree_insert(tree_node *node, int data);

int tree_size(tree_node *node);

int tree_max_depth(tree_node *node);

int tree_minimum_value(tree_node *node);

int tree_maximum_value(tree_node *node);

void tree_print_increasing(tree_node *node);

void tree_print_post_order(tree_node *node);

void tree_print_reversed(tree_node *node);

bool tree_is_bst(tree_node *node);

tree_node *p_build_123_a();

tree_node *p_build_123_b();

tree_node *p_build_123_c();
