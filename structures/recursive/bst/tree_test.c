#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "tree.h"

static void test_create_tree()
{
    tree_node *node = tree_create(1);
    assert_int_equal(node->data, 1);
    assert_null(node->left);
    assert_null(node->right);
}

static void test_tree_lookup()
{
    assert_false(tree_lookup(NULL, 4));
    tree_node *node = tree_create(5);
    assert_false(tree_lookup(node, 4));
    assert_true(tree_lookup(node, 5));
}

static void test_tree_insert()
{
    tree_node *node_1 = tree_insert(NULL, 10);
    assert_int_equal(node_1->data, 10);

    tree_node *node_2 = tree_create(100);
    tree_insert(node_2, 20);
    tree_insert(node_2, 110);

    assert_int_equal(node_2->left->data, 20);
    assert_int_equal(node_2->right->data, 110);
}

static void test_tree_size()
{
    tree_node *node = tree_insert(NULL, 1);

    assert_int_equal(tree_size(node), 1);

    tree_insert(node, 10);

    assert_int_equal(tree_size(node), 2);

    tree_insert(node, 48);
    tree_insert(node, 1);
    tree_insert(node, 63);
    tree_insert(node, 5);

    assert_int_equal(tree_size(node), 6);
}

static void test_max_depth()
{
    tree_node *node = tree_insert(NULL, 20);

    assert_int_equal(tree_max_depth(node), 1);

    // Test left
    tree_insert(node, 19);
    tree_insert(node, 18);
    tree_insert(node, 17);

    assert_int_equal(tree_max_depth(node), 4);

    // Then right
    tree_insert(node, 21);
    tree_insert(node, 22);
    tree_insert(node, 23);
    tree_insert(node, 24);
    tree_insert(node, 25);

    assert_int_equal(tree_max_depth(node), 6);
}

static void test_tree_minimum()
{
    tree_node *tree = tree_insert(NULL, 9);

    assert_int_equal(tree_minimum_value(tree), 9);

    tree_insert(tree, 8);

    assert_int_equal(tree_minimum_value(tree), 8);
}

static void test_tree_maximum()
{
    tree_node *tree = tree_insert(NULL, 10);

    assert_int_equal(tree_maximum_value(tree), 10);

    tree_insert(tree, 20);

    assert_int_equal(tree_maximum_value(tree), 20);
}

static void test_create_123_a()
{
    tree_node *tree = p_build_123_a();
    assert_int_equal(tree->data, 2);
    assert_int_equal(tree->left->data, 1);
    assert_int_equal(tree->right->data, 3);
}

static void test_create_123_b()
{
    tree_node *tree = p_build_123_b();
    assert_int_equal(tree->data, 2);
    assert_int_equal(tree->left->data, 1);
    assert_int_equal(tree->right->data, 3);
}

static void test_create_123_c()
{
    tree_node *tree = p_build_123_c();
    assert_int_equal(tree->data, 2);
    assert_int_equal(tree->left->data, 1);
    assert_int_equal(tree->right->data, 3);
}

static void test_tree_is_bst()
{
    tree_node *tree = tree_insert(NULL, 10);
    tree_insert(tree, 11);
    tree_insert(tree, 9);

    assert_true(tree_is_bst(tree));

    tree->left->data = 20;

    assert_false(tree_is_bst(tree));

    tree->left->data = 9;
    tree->right->data = 9;

    assert_false(tree_is_bst(tree));
}

int main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_create_tree),
        cmocka_unit_test(test_tree_lookup),
        cmocka_unit_test(test_tree_insert),
        cmocka_unit_test(test_tree_size),
        cmocka_unit_test(test_max_depth),
        cmocka_unit_test(test_tree_minimum),
        cmocka_unit_test(test_tree_maximum),
        cmocka_unit_test(test_tree_is_bst),
        cmocka_unit_test(test_create_123_a),
        cmocka_unit_test(test_create_123_b),
        cmocka_unit_test(test_create_123_c),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
