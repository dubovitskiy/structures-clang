#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "tree.h"

// Helper function that initializes a new tree node
tree_node *tree_create(int data)
{
    tree_node *node = (tree_node *)malloc(sizeof(tree_node));

    node->data = data;
    node->left = NULL;
    node->right = NULL;

    return node;
}

// Return true if the given data is present
// inside the tree
bool tree_lookup(tree_node *node, int data)
{
    // Return false if the tree is empty
    if (node == NULL)
    {
        return false;
    }

    // Return true if current node data is
    // equals to the given one
    if (node->data == data)
    {
        return true;
    }

    // Call lookup for parent
    if (data < node->data)
    {
        return tree_lookup(node->left, data);
    }
    else
    {
        return tree_lookup(node->right, data);
    }
}

// Insert new node into the tree
tree_node *tree_insert(tree_node *node, int data)
{
    // Return new tree node if the given tree is null
    if (node == NULL)
    {
        return tree_create(data);
    }

    if (data < node->data)
    {
        node->left = tree_insert(node->left, data);
    }
    else
    {
        node->right = tree_insert(node->right, data);
    }

    return node;
}

int tree_size(tree_node *node)
{
    if (node == NULL)
    {
        return 0;
    }

    return tree_size(node->left) + 1 + tree_size(node->right);
}

int tree_max_depth(tree_node *node)
{
    if (node == NULL)
    {
        return 0;
    }

    int left_depth = tree_max_depth(node->left);
    int right_depth = tree_max_depth(node->right);

    // Return max
    if (left_depth > right_depth)
    {
        return left_depth + 1;
    }
    else
    {
        return right_depth + 1;
    }
}

// Get a value of the last left subtree node
int tree_minimum_value(tree_node *node)
{
    tree_node *current = node;

    while (current->left != NULL)
    {
        current = current->left;
    }

    return current->data;
}

// Get a value of the right subtree node
int tree_maximum_value(tree_node *node)
{
    tree_node *current = node;

    while (current->right != NULL)
    {
        current = current->right;
    }

    return current->data;
}

void tree_print_increasing(tree_node *node)
{
    if (node == NULL)
    {
        return;
    }

    tree_print_increasing(node->left);
    printf("%d\n", node->data);
    tree_print_increasing(node->right);
}

void tree_print_post_order(tree_node *node)
{
    if (node == NULL)
    {
        return;
    };

    tree_print_post_order(node->left);
    tree_print_post_order(node->right);
    printf("%d ", node->data);
}

void tree_print_reversed(tree_node *node)
{
    if (node == NULL)
    {
        return;
    }

    tree_print_reversed(node->right);
    printf("%d ", node->data);
    tree_print_reversed(node->left);
}

bool tree_is_bst(tree_node *node)
{
    // Three base cases

    if (node == NULL)
    {
        return true;
    }

    if (node->left != NULL && node->left->data > node->data)
    {
        return false;
    }

    if (node->right != NULL && node->right->data < node->data)
    {
        return false;
    }

    if (!tree_is_bst(node->left) || !tree_is_bst(node->right))
    {
        return false;
    }

    return true;
}

// Problems

tree_node *p_build_123_a()
{
    tree_node *root = tree_create(2);
    tree_node *left = tree_create(1);
    tree_node *right = tree_create(3);

    root->left = left;
    root->right = right;

    return root;
}

tree_node *p_build_123_b()
{
    tree_node *root = tree_create(2);

    root->left = tree_create(1);
    root->right = tree_create(3);

    return root;
}

tree_node *p_build_123_c()
{
    tree_node *root = tree_insert(NULL, 2);

    tree_insert(root, 1);
    tree_insert(root, 3);

    return root;
}
